import axios from "axios";
import { ProductInterface } from '../interfaces/product.interface'

const API_URL = import.meta.env.VITE_APP_API_URL;
const URL = () => `${API_URL}/shopify`;

export async function getProducts() : Promise<ProductInterface[]> {
    const { data } = await axios.get(`${URL()}`);
    return data.products;
  }