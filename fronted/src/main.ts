
import { createApp } from 'vue'
import './style.css'
import axios from "axios";
import VueAxios from "vue-axios";
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
import App from './App.vue'

const app = createApp(App);

app
    .use(VueAxios, axios)
    .use(Antd)
    .mount("#app")

