import * as dotenv from 'dotenv';
import express from "express";
import { Request, Response, NextFunction } from "express";
const app = express();
dotenv.config();
import helmet from "helmet";
import bodyParser from "body-parser";
import cors from 'cors';
import { configCors } from './utils/cors';

import ShopifyRoute from "./routes/ShopifyRoute";
import morgan from "morgan";

app.use(helmet());
app.use(bodyParser.json());

app.use(morgan<Request, Response>("dev"));

app.use("/shopify", ShopifyRoute);

app.get('/hello', (req, res)=> {
  res.send('Hello')
})

app.use((error: any, res: Response, next: NextFunction) => {
  try {
    res.status(404).send("Resource not found");
  } catch (error) {
    next(error);
  }
});

app.use((error: any, res: Response, next: NextFunction) => {
  try {
    const status = error.status || 500;
    const message =
      error.message ||
      "There was an error while processing your request, please try again";
    return res.status(status).send({
      status,
      message,
    });
  } catch (error) {
    next(error);
  }
});

const port = process.env.PORT || 5000;

app.listen(port, () => {
  console.log(`Application started on ${port}...`);
});

export default app;