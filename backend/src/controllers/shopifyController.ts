import { Request, Response, NextFunction } from "express";
import createError from "http-errors";
import { getProducts as getProductsService } from "../services/shopify";

export const getProducts = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const products = await getProductsService();
    res.status(200).json({
      products: products,
    });
  } catch (error: any) {
    return next(
      res.status(400).json({
        message: "Sucedio un error.",
      })
    );
  }
};
