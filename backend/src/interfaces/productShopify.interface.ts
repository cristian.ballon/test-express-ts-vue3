export interface ProductInterface {
  id: number;
  title: string;
  body_html: string;
  vendor: string;
  product_type: string;
  created_at: string;
  status: string;
  admin_graphql_api_id: string;
  published_scope: string;
  image: ImagesInterface;
  images: ImagesInterface[];
  variants: VariantInterface[]
}

export interface VariantInterface {
  id: number;
  product_id: number;
  title: string;
  price: string;
  sku: string;
  position: number;
  inventory_policy: string;
  compare_at_price: string;
}

export interface ImagesInterface {
  id: number;
  product_id: number;
  position: number;
  width: number;
  height: number;
  src: string;
  admin_graphql_api_id: string;
}
