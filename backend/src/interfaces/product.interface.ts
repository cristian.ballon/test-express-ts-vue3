export interface ProductInterface {
  id: number;
  title: string;
  bodyHtml: string;
  vendor: string;
  productType: string;
  createdAt: string;
  status: string;
  adminGraphqlApiId: string;
  publishedScope: string;
  image: ImagesInterface;
  images: ImagesInterface[];
  variants: VariantInterface[]
}

export interface VariantInterface {
  id: number;
  productId: number;
  title: string;
  price: string;
  sku: string;
  position: number;
  inventoryPolicy: string;
  compareAtPrice: string;
}

export interface ImagesInterface {
  id: number;
  productId: number;
  position: number;
  width: number;
  height: number;
  src: string;
  adminGraphqlApiId: string;
}
