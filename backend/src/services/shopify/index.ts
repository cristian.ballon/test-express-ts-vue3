const { SHOPIFY_API_URL, SHOPIFY_API_KEY, SHOPIFY_API_HEADER } = process.env;
import { ProductInterface } from '../../interfaces/product.interface'
import Axios from 'axios';
import { configGetProducts } from './axios.config'
import { productsTransform  } from '../../transforms/productTramsform'

export const getProducts = async () : Promise<ProductInterface[]> => {
    try {
        const { data }  = await Axios(configGetProducts)
        return productsTransform(data.products)
    }catch (err: any) {
        if(err.response && err.response.status === 402) {
            console.error("Unavailable Shop ","ERROR SHOPIFY PAYMENT REQUIRED")
        }
        return []
    }
    
}