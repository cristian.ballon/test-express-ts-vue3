const { SHOPIFY_API_URL, SHOPIFY_API_KEY, SHOPIFY_API_HEADER } = process.env;
import { AxiosRequestConfig } from 'axios';

export const configGetProducts: AxiosRequestConfig = {
    method: 'get',
    url: `${SHOPIFY_API_URL}/2022-10/products.json`,
    headers: {
        'X-Shopify-Access-Token': `${SHOPIFY_API_KEY}`
    }
}
