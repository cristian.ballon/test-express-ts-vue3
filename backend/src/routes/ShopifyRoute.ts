import { Router } from "express";
import cors from 'cors';
import { configCors } from '../utils/cors';
const router: Router = Router();
import * as ShopifyController from "../controllers/shopifyController";

router.get("", cors(configCors), ShopifyController.getProducts);

export default router;