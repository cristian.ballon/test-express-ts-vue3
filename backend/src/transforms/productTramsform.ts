import {ProductInterface, ImagesInterface, VariantInterface} from '../interfaces/productShopify.interface'
import {ProductInterface as ProductInterfaceModel, ImagesInterface as ImagesInterfaceModel, VariantInterface as VariantInterfaceModel} from '../interfaces/product.interface'

const productTransform = (record: ProductInterface) : ProductInterfaceModel => {
   const result: ProductInterfaceModel = {
        id: record.id,
        title: record.title,
        bodyHtml: record.body_html,
        vendor: record.vendor,
        productType: record.product_type,
        createdAt: record.created_at,
        status: record.status,
        adminGraphqlApiId: record.admin_graphql_api_id,
        publishedScope: record.published_scope,
        image: imageTranform(record.image),
        images: imagesTranform(record.images),
        variants: variantsTranform(record.variants)
   }

   return result;
}

const imagesTranform = (records: ImagesInterface[]):ImagesInterfaceModel[] => {
    const result: ImagesInterfaceModel[] = []
    for (const record of records) {
        result.push(imageTranform(record))
    }
    return result;
}

const imageTranform = (image: ImagesInterface):ImagesInterfaceModel => {
    const result: ImagesInterfaceModel = {
        id: image.id,
        productId: image.product_id,
        position: image.position,
        width: image.width,
        height: image.height,
        src: image.src,
        adminGraphqlApiId: image.admin_graphql_api_id
    }

    return result;
}

const variantsTranform = (records: VariantInterface[]):VariantInterfaceModel[] => {
    const result: VariantInterfaceModel[] = []
    for (const record of records) {
        result.push(variantTranform(record))
    }
    return result;
}

const variantTranform = (variant: VariantInterface): VariantInterfaceModel => {
    const result: VariantInterfaceModel = {
        id: variant.id,
        productId: variant.product_id,
        title: variant.title,
        price: variant.price,
        sku: variant.sku,
        position: variant.position,
        inventoryPolicy: variant.inventory_policy,
        compareAtPrice: variant.compare_at_price
    }

    return result;
}

export const productsTransform = (records: ProductInterface[]) : ProductInterfaceModel[] => {
    const result: ProductInterfaceModel[] = [];
    for (const record of records) {
        result.push(productTransform(record));
    }
    return result;
}