const { URL_FRONTED } = process.env;
import cors, { CorsOptions } from "cors";
const whitelist = [URL_FRONTED, 'http://localhost:5000'];

export const configCors: CorsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin as string) !== -1) {
      callback(null, true);
    } else {
      callback(new Error("Not allowed by CORS"));
    }
  },
};
